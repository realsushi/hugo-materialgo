#### Web Hugo theme based on Material Design framework principles.
This is a hugo theme to rebuild some of my websites. The goal is to improve content creation and ease maintainability.
##### Key aspects
* Html fitting partials
* Css from framework
* Composition of extra Css components

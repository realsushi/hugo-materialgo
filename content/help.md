+++
title= "help"
date= "2019-09-19T16:58:58-05:00"
Description= "Comment utiliser le site"
Tags= ["help"]
Categories= ["help"]
Display= ["false"]
+++
##### Comment utiliser le theme?
Allez sur la page d'accueil en cliquant sur accueil.

##### Comment passer un argument à une page:
Le . contient les variables Golang et front-matter de Hugo.
On, peut donc transmettre ""."" en le passant en paramètre.
ex :  [[partial "lambda.hmlt" .]]

+++
title= "plan web"
date= "2019-09-25T16:58:58-05:00"
Description= "plan du site web de la mairie"
Tags= ["site"]
Categories= ["plan"]
draft=false
+++

# Plan du site

<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xhtml="http://www.w3.org/1999/xhtml">

  <url>
    <loc>http:/categories/</loc>
    <lastmod>2019-09-24T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/tags/ev/</loc>
    <lastmod>2019-09-24T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/categories/evenement/</loc>
    <lastmod>2019-09-24T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/evt/</loc>
    <lastmod>2019-09-24T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/</loc>
    <lastmod>2019-09-24T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/evt/evt7/</loc>
    <lastmod>2019-09-24T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/tags/</loc>
    <lastmod>2019-09-24T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/cal/cal/</loc>
    <lastmod>2019-09-23T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/tags/asso/</loc>
    <lastmod>2019-09-23T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/asso/asso0/</loc>
    <lastmod>2019-09-23T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/categories/association/</loc>
    <lastmod>2019-09-23T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/asso/</loc>
    <lastmod>2019-09-23T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/tags/cal/</loc>
    <lastmod>2019-09-23T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/categories/calendrier/</loc>
    <lastmod>2019-09-23T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/cal/</loc>
    <lastmod>2019-09-23T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/evt/evt6/</loc>
    <lastmod>2019-09-23T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/evt/evt5/</loc>
    <lastmod>2019-09-21T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/evt/evt4/</loc>
    <lastmod>2019-09-20T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/orga/orga2/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/orga/orga4/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/categories/article/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/tags/article/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/asso/asso1/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/asso/asso2/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/asso/asso3/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/categories/carte/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/carte/carte/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/carte/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/post/post2/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/tags/debug/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/evt/evt0/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/orga/orga1/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/orga/orga3/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/tags/map/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/meteo/meteo/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/meteo/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/tags/mto/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/categories/mto/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/tags/orga/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/orga/orga0/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/orga/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/post/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/post/post1/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/evt/evt3/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/evt/evt2/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/evt/evt1/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

  <url>
    <loc>http:/categories/structure/</loc>
    <lastmod>2019-09-19T16:58:58-05:00</lastmod>
  </url>

</urlset>

##### Idées:
###### Pour chaque list-page on créé une regular page appelée list0
> Creation d'une variable
###### Structure de list-page
Content page zéro [Calendrier/plan/orga] puis liste


##### Idées pour Captain Future:
Récuperer le texte wikipédia et le structurer dans un post en Md.

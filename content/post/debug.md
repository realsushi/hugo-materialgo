+++
title= "GO_DEBUG"
date= "2019-09-19T16:58:58-05:00"
Description= "debug"
Tags= ["debug","test","bug"]
Categories= ["debug"]
draft= true
+++

<a href="/">Icon</a>
<br><br>
<p>Tags: {{ delimit .Params.tags ", " }}</p>
<br><br>
# debug
{{ range first 10 .Data.Pages }}
  <a href={{ .Permalink }}>{{.Title}}</a>
  {{range first 3 .Data.Pages.Params}}
  <a [[.Params.Description]]</a>
  {{end}}
{{ end }}
## debug post for meta
nombre articles :
nombre de catégories :
article 1 : titre / type / date [date ojd] / has_content
article 2 ...

# Variable | Header One     | Header Two     |
| :------------- | :------------- |
| Item One       | Item Two       |

.Permalink {{.Permalink}}
.File.BaseFileName {{.File.BaseFileName}}


# Sitemap layout
test: A---
          |-----A1
          |-----A2
      B---
          |-----B1

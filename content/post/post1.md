+++
title= "premier article"
date= "2019-09-19T16:58:58-05:00"
Description= "article de base"
Tags= ["article"]
Categories= ["article"]
+++

# this is Post1
It is actully empty but it is likely to change very soon.

## What is expected
Feel free to contribute.
Ty.

With this the same piece of content can appear in different positions in different taxonomies.
## this is title 2
Currently taxonomies only support the default ordering of content which is weight -> date.

There are two different templates that the use of taxonomies will require you to provide.

Both templates are covered in detail in the templates section.

A list template is any template that will be used to render multiple pieces of content in a single html page. This template will be used to generate all the automatically created taxonomy pages.

A taxonomy terms template is a template used to generate the list of terms for a given template.

There are four common ways you can display the data in your taxonomies in addition to the automatic taxonomy pages created by hugo using the list templates:

    For a given piece of content, you can list the terms attached
    For a given piece of content, you can list other content with the same term
    You can list all terms for a taxonomy
    You can list all taxonomies (with their terms)
